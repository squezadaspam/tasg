﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverScriptController : MonoBehaviour
{
    public GameObject GameOverScreen;
    private MainGameControllerCore mainGameController;
    // Start is called before the first frame update
    void Start()
    {
        mainGameController = GameObject.Find("MainGameControllerCore").GetComponent<MainGameControllerCore>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Die()
    {
        GameOverScreen.SetActive(true);
        GameObject.FindGameObjectWithTag("Player").SetActive(false);
    }
}

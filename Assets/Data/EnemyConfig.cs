﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Enemy Config", menuName = "Enemy Config", order = 0)]
public class EnemyConfig : ScriptableObject
{
    public SpriteRenderer enemySprite;
    public Vector3 enemyDirection;
    public float enemySpeed;
    public bool canShoot;
    public bool moveToSpecificPoint;
    public bool randomSpecificPosition;
    public Vector3 specificPosition;
}

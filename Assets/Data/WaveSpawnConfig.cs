﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Wave Spawn Config", menuName = "Wave Spawn Config", order = 0)]
public class WaveSpawnConfig : ScriptableObject
{
    [System.Serializable]
    public class EnemyConfig
    {
        public EnemyConfigController enemyController;
        public Vector3 specificPositionSpawn;
        public bool useRandomPosition;
        public int enemyQuantity;
        public float enemySpawnCadence;
    }

    [System.Serializable]
    public class BossConfig
    {
        public EnemyConfigController enemyController;
        public Vector3 specificPositionSpawn;
        public bool useRandomPosition;
        public int enemyQuantity;
        public float enemySpawnCadence;
    }
    public List<EnemyConfig> enemies;
    public List<BossConfig> bosses;
    public float waveCandence;
    public float startWaveAtSeconds;
    public float startBossAtSeconds;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiOptionsController : MonoBehaviour
{
    private MainGameControllerCore mainGameController;
    // Start is called before the first frame update
    void Start()
    {
        mainGameController = GameObject.Find("MainGameControllerCore").GetComponent<MainGameControllerCore>();

    }

    public void GoToStart()
    {
        mainGameController.GoToStartScreen();
    }

    public void ChangeMasterVol(float value) {
        mainGameController.AudioMMasterController(value);
    }
    public void ChangeFXVol(float value)
    {
        mainGameController.AudioMFXController(value);
    }
    public void ChangeBackVol(float value)
    {
        mainGameController.AudioMBackgorundController(value);    }
}

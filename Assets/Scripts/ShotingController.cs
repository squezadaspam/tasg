﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotingController : MonoBehaviour
{
    private float fireRate = 0.3f;
    private float lifeTime = 2f;
    private float bulletSpeed = 10f;
    private float baseDamage = 1f;

    private float nextFire;

    private ObjectPooler bulletPooler;
    // Start is called before the first frame update
    void Start()
    {
        bulletPooler = GameObject.Find("BulletPool").GetComponent<ObjectPooler>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool CanFire()
    {
        if (Time.time > nextFire)
        {
            return true;
        }
        return false;
    }

    private void ResetShootTime()
    {
        nextFire = Time.time + fireRate;
    }

    public void Fire(Transform spawnPosition)
    {
        if (bulletPooler)
        {
            GameObject bullet = bulletPooler.getPooledObject();
            if (bullet == null) return;
            bullet.GetComponent<Bullet>().Setup(spawnPosition.position, lifeTime, bulletSpeed);
            bullet.SetActive(true);
        }
        ResetShootTime();
        //Debug.Log("FIRE!");
    }
}

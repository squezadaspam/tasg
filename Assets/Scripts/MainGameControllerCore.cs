﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class MainGameControllerCore : MonoBehaviour
{
    public float speed;
    public float candence;
    public float power;

    public AudioMixer audioMixer;

    private PlayerAttributes player;
    private SceneController sceneController;

    // Se declaran las variables tipo InputAction para la asignación de las acciones de entrada

    private GameObject pauseWindow;

    private bool isOnPause = false;

    private void Start()
    {
        pauseWindow = GameObject.Find("PauseWindowParent");
        sceneController = GetComponent<SceneController>();
        GetComponent<SoundsSceneController>().PlaySceneSound("Start");
    }

    void Awake()
    {

        //Let the gameobject persist over the scenes
        DontDestroyOnLoad(gameObject);
    }

    void loadPlayerData()
    {

    }

    private void OnLevelWasLoaded(int level)
    {
        pauseWindow = GameObject.Find("PauseWindowParent");
    }

    private void Update()
    {
        if (Keyboard.current.escapeKey.wasPressedThisFrame)
        {
            TogglePause();
        }
    }

    internal void GoToOptionScreen()
    {
        sceneController.LoadOptionsScene();
    }

    public void StartNewGame()
    {
        player = new PlayerAttributes();
        player.Speed = this.speed;
        player.Candence = this.candence;
        player.Damage = this.power;

        sceneController.LoadMainGame();
    }

    void PlusPlayerAttr(string attr, float value)
    {
        if(attr!="" && value!=0)
        {
            switch (attr)
            {
                case "speed":
                    this.player.Speed += value;
                    this.player.SpeedCount += 1;
                    break;

                case "power":
                    this.player.Damage += value;
                    this.player.DamageCount += 1;
                    break;

                case "candence":
                    this.player.Candence += value;
                    this.player.CandenceCount += 1;
                    break;

            }
        }
    }

    public PlayerAttributes GetPlayerAttributes()
    {
        PlayerAttributes player = this.player;
        if (player == null)
        {
            return new PlayerAttributes();
        }
        return player;
    }

    public void GoToStartScreen()
    {
        sceneController.LoadStartScene();
    }

    public void GoToGarageScreen()
    {
        sceneController.LoadGarageScene();
    }


    private void TogglePause()
    {
        if (isOnPause)
        {
            Time.timeScale = 1;
            isOnPause = false;
            pauseWindow.transform.GetChild(0).gameObject.SetActive(false);
            return;
        }
        Time.timeScale = 0;
        isOnPause = true;
        pauseWindow.transform.GetChild(0).gameObject.SetActive(true);
    }

    public void AudioMBackgorundController(float val)
    {
        audioMixer.SetFloat("Back", val);
    }
    public void AudioMFXController(float val)
    {
        audioMixer.SetFloat("FX", val);
    }
    public void AudioMMasterController(float val)
    {
        audioMixer.SetFloat("Master", val);
    }
}

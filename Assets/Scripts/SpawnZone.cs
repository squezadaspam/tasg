﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnZone : MonoBehaviour
{
    [SerializeField]
    private float xSize = 1f;

    [SerializeField]
    private float ySize = 1f;

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawCube(transform.position, new Vector3(xSize, ySize, 1));
    }

    public Vector3 GetRandomPosition()
    {
        Vector3 randomPoint = new Vector3(transform.position.x + Random.Range((xSize/2) * -1, (xSize/2)), transform.position.y + Random.Range((ySize/2) * -1, (ySize/2)), 0);
        return randomPoint;
        
        //Debug.Log(position);
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

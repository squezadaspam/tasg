﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputController : MonoBehaviour, PlayerInputControllerTasg.IPlayerActions
{
    // Se declaran las variables tipo InputAction para la asignación de las acciones de entrada
    private PlayerInputControllerTasg playerControllerTasg;

    // Se declara la clase controlladora del personaje principal
    private PlayerController playerController;

    private void Awake()
    {
        playerControllerTasg = new PlayerInputControllerTasg();
        playerControllerTasg.Player.SetCallbacks(this);
    }

    private void OnEnable()
    {
        playerControllerTasg.Player.Enable();

    }

    private void OnDisable()
    {
        playerControllerTasg.Player.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        // se instancia el objeto al iniciar el ciclo Start
        playerController = gameObject.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        Move(); // Mover al personaje
        Fire();
    }

    // Utiliza la clase controladora para mover al personaje enviando los datos del input wasdInput.
    private void Move()
    {
        playerController.MoveIfCan();
    }

    private void Fire()
    {
        playerController.Shoot();
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        playerController.moveDirectionInput = context.ReadValue<Vector2>();
    }

    public void OnShot(InputAction.CallbackContext context)
    {
        playerController.shotInput = context.ReadValue<float>();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnInstantiate : MonoBehaviour
{
    [SerializeField]
    private GameObject preFab;

    public void InstantiateThis()
    {
        Instantiate(preFab, transform.position, transform.rotation);
    }
}

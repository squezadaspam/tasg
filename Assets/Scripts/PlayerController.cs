﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Animations;
using UnityEngine;

/**
 * Clase que administra todas las interacciones del Player
 * (Ataque, Movimiento, Recolección de objetos, muerte, etc)
 */
public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    private PlayerAttributes playerAttributes;

    private ShotingController shotingController;

    [SerializeField]
    private AudioClip shoot;

    private AudioSource audioSource;
    /**
     * Se establecen las cordenadas límites de la zona de juego
     */
    private float screenLimitsX = 8.7f;
    private float screenLimitsY = 4.7f;

    /**
     * Variable que recive el valor del input asignada en ¨PlayerInputcontroller
     */
    [HideInInspector]
    public Vector2 moveDirectionInput;

    [HideInInspector]
    public float shotInput;


    [HideInInspector]
    public Animator playerAnimations;

    //============================== SERIALIZED FIELDS


    [SerializeField]
    private Transform bulletSpawn;

    private void Awake()
    {
        playerAttributes = new PlayerAttributes();
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        playerAnimations = GetComponent<Animator>();
        shotingController = GetComponent<ShotingController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Move()
    {
        Vector2 velocity = (moveDirectionInput * playerAttributes.Speed * Time.deltaTime);
        transform.position += new Vector3(velocity.x, velocity.y, 0);
    }

    /**
     * Devuelve el 
     */
    public void MoveIfCan()
    {
        if (CanMove())
        {
            Vector2 velocity = (moveDirectionInput * playerAttributes.Speed * Time.deltaTime);
            transform.position += new Vector3(velocity.x, velocity.y, 0);
        } else
        {
            FixToPlayableArea2();
        }

        if(moveDirectionInput.y == 0 && moveDirectionInput.x == 0)
        {
            AnimateIdle();
        } else
        {
            AnimateMovement();
        }
        
    }

    /**
     * Nombre: CanMove
     * Return: bool
     * Descripcion: Verifica si el jugador se encuentra dentro del área permitida y si este se puede mover. Devuelve true si es así, False si no.
     * =====================================================
     */
    private bool CanMove()
    {
        bool flag = false;

        if (Mathf.Abs(transform.position.x) <= screenLimitsX && Mathf.Abs(transform.position.y) <= screenLimitsY)
            flag = true;

        return flag;
    }

    /**
     * Nombre: FixToPlayableArea
     * Return: Void
     * Descripcion: Si el juegador se encuentra fuera del área permitida, lo mueve dentro.
     * =====================================================
     */
    private void FixToPlayableArea()
    {
        float absX = Mathf.Abs(transform.position.x);
        float absY = Mathf.Abs(transform.position.y);
        float newFixedX = screenLimitsX - 0.1f;
        float newFixedY = screenLimitsY - 0.1f;

        if (absX >= screenLimitsX)
        {
            if (transform.position.x < 0)
            {
                newFixedX = (newFixedX) * -1f;  
            }
            transform.position = new Vector3(newFixedX, transform.position.y, 0);
        }

        if (absY >= screenLimitsY)
        {
            if (transform.position.y < 0)
            {
                newFixedY = (newFixedY) * -1f;
            }
            transform.position = new Vector3(transform.position.x, newFixedY, 0);
        }

        
    }

    private void FixToPlayableArea2()
    {
        float fixedX = Mathf.Clamp(transform.position.x, screenLimitsX * -1, screenLimitsX);
        float fixedY = Mathf.Clamp(transform.position.y, screenLimitsY * -1, screenLimitsY);
        transform.position = new Vector3(fixedX, fixedY, 0);
    }

    public void Shoot()
    {
        if(shotInput == 1)
        {
            if(shotingController.CanFire()){
                audioSource.PlayOneShot(shoot);
                shotingController.Fire(bulletSpawn);
            }
        }
        
    }

    private void AnimateMovement()
    {
        playerAnimations.SetBool("OnMovement", true);
    }

    private void AnimateIdle()
    {
        playerAnimations.SetBool("OnMovement", false);
    }
}

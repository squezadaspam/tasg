﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    private SoundsSceneController soundsSceneController;

    private void Start()
    {
        soundsSceneController = GetComponent<SoundsSceneController>();
    }
    public void LoadMainGame()
    {
        SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
        soundsSceneController.PlaySceneSound("FirstLevel");
    }
    public void LoadGarageScene()
    {
        SceneManager.LoadScene("GarageScene", LoadSceneMode.Single);
        soundsSceneController.PlaySceneSound("Garage");
    }

    public void LoadStartScene()
    {
        SceneManager.LoadScene("StartScene", LoadSceneMode.Single);
        //soundsSceneController.PlaySceneSound("Start");
    }

    public void LoadOptionsScene()
    {
        SceneManager.LoadScene("OptionScene", LoadSceneMode.Single);
    }

}

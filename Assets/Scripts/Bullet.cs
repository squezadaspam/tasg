﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float lifeTime = 1f;
    private float bulletSpeed = 3f;


    private void FixedUpdate()
    {
        transform.position += transform.up * bulletSpeed * Time.deltaTime;
    }

    void DisableBullet()
    {
        //Debug.Log("disabled");
        gameObject.SetActive(false);
    }

    public void Setup(Vector2 startPosition, float lifeTime, float bulletSpeed)
    {
        //Debug.Log("Position:" + startPosition + " lifetime:" + lifeTime + " bulletSpeed:" + bulletSpeed);
        this.lifeTime = lifeTime;
        this.bulletSpeed = bulletSpeed;
        transform.position = startPosition;
    }

    private void OnEnable()
    {
        Invoke("DisableBullet", lifeTime);
    }
}

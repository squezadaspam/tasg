﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using UnityEngine;
using UnityEngine.Events;

public class OnTrigger : MonoBehaviour
{
    [SerializeField]
    private UnityEvent action;

    [SerializeField]
    private bool EnemyImpact;

    [SerializeField]
    private bool EnemyBulletImpact;

    [SerializeField]
    private bool PlayerBulletImpact;

    [SerializeField]
    private bool AllImpact;

    private GameObject collisionObject;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        collisionObject = collision.gameObject;
        if (EnemyBulletImpact)
        {
            ThisEnemyAndPlayerBulletImpact(collisionObject);
        }
        if (PlayerBulletImpact)
        {
            ThisPlayerAndEnemyBulletImpact(collisionObject);
        }
        if (EnemyImpact)
        {
            ThisPlayerAndEnemyImpact(collisionObject);
        }
        if (AllImpact)
        {
            action.Invoke();
        }
    }

    public void DisableCollisionObject()
    {
        if (collisionObject)
        {
            collisionObject.SetActive(false);
        }
    }

    private void ThisEnemyAndPlayerBulletImpact(GameObject collisionObject) {
        if (collisionObject.tag == "PlayerBullet" && this.gameObject.tag == "Enemy")
        {
            OnPlayerBulletImpact();
            action.Invoke();
        }
    }
    private void ThisPlayerAndEnemyBulletImpact(GameObject collisionObject)
    {
        if (collisionObject.tag == "EnemyBullet" && this.gameObject.tag == "Player")
        {
            OnEnemyBulletImpact();
            action.Invoke();
        }
    }

    private void ThisPlayerAndEnemyImpact (GameObject collisionObject)
    {
        if (collisionObject.tag == "Enemy" && this.gameObject.tag == "Player")
        {
            OnEnemyImpact();
            action.Invoke();
        }
    }

    private void OnEnemyImpact()
    {
        
    }

    private void OnPlayerBulletImpact()
    {

    }

    private void OnEnemyBulletImpact()
    {

    }
}

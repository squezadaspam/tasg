﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIGarageScreenController : MonoBehaviour
{
    [SerializeField]
    private Button backButton;

    [SerializeField]
    private Button addSpeedButton;

    [SerializeField]
    private Button addPowerButton;

    [SerializeField]
    private Button addCandenceButton;

    private MainGameControllerCore mainGameController;
    // Start is called before the first frame update
    void Start()
    {
        mainGameController = GameObject.Find("MainGameControllerCore").GetComponent<MainGameControllerCore>();

        backButton.onClick.AddListener(Back);
        addSpeedButton.onClick.AddListener(AddSpeed);
        addPowerButton.onClick.AddListener(AddPower);
        addCandenceButton.onClick.AddListener(AddCandence);
    }
    private void Back()
    {
        mainGameController.GoToStartScreen();
    }

    private void AddSpeed()
    {
        Debug.Log("Speed +1");
    }

    private void AddPower()
    {
        Debug.Log("Power +1");
    }

    private void AddCandence()
    {
        Debug.Log("Candence +1");
    }
}

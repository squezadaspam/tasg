﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDelete : MonoBehaviour
{
    public void DeleteThis()
    {
        Destroy(gameObject);
    }
}

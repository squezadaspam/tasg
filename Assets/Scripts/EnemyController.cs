﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField]
    private List<ObjectPooler> enemyPoolList;

    private SpawnZone enemySpawnL;
    private SpawnZone enemySpawnR;
    private SpawnZone enemySpawnT;
    private SpawnZone enemyArea;

    void Start()
    {
        enemySpawnL = GameObject.FindWithTag("EnemySpawnL").GetComponent<SpawnZone>();
        enemySpawnR = GameObject.FindWithTag("EnemySpawnR").GetComponent<SpawnZone>();
        enemySpawnT = GameObject.FindWithTag("EnemySpawnT").GetComponent<SpawnZone>();
        enemyArea = GameObject.FindWithTag("EnemyArea").GetComponent<SpawnZone>();

    }

    public void SpawnEnemy()
    {
        Vector2 randomPosition = new Vector2(0, 0);
        float percent = Random.Range(1, 4);
        Debug.Log("range: " + percent);
        switch (percent)
        {
            case 1:
                randomPosition = enemySpawnL.GetRandomPosition();
                 break;

            case 2:
                randomPosition = enemySpawnR.GetRandomPosition();
                break;

            case 3:
                randomPosition = enemySpawnT.GetRandomPosition();
                break;
        }
        GameObject obj = enemyPoolList[0].getPooledObject();
        obj.transform.position = randomPosition;
        obj.SetActive(true);

    }
}


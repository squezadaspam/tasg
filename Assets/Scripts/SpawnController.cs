﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private WaveSpawnConfig waveSpawnData;

    [SerializeField]
    private List<ObjectPooler> enemyPoolList;

    [SerializeField]
    private List<ObjectPooler> bossPoolList;

    private SpawnZone enemySpawnL;
    private SpawnZone enemySpawnR;
    private SpawnZone enemySpawnT;
    private SpawnZone enemyArea;

    public void Start()
    {
        enemySpawnL = GameObject.FindWithTag("EnemySpawnL").GetComponent<SpawnZone>();
        enemySpawnR = GameObject.FindWithTag("EnemySpawnR").GetComponent<SpawnZone>();
        enemySpawnT = GameObject.FindWithTag("EnemySpawnT").GetComponent<SpawnZone>();
        enemyArea = GameObject.FindWithTag("EnemyArea").GetComponent<SpawnZone>();

        StartCoroutine(StartWaves());
    }

    private IEnumerator StartWaves()
    {
        yield return new WaitForSeconds(waveSpawnData.startWaveAtSeconds);

        foreach (var enemy in waveSpawnData.enemies)
        {

            for (int i = 0; i < enemy.enemyQuantity; i++)
            {
                SpawnEnemy(enemy.useRandomPosition, enemy.specificPositionSpawn, enemy.enemyController);
                yield return new WaitForSeconds(enemy.enemySpawnCadence);
            }
            
            yield return new WaitForSeconds(waveSpawnData.waveCandence);
        }
        yield return new WaitForSeconds(waveSpawnData.startBossAtSeconds);
        foreach (var boss in waveSpawnData.bosses)
        {

            for (int i = 0; i < boss.enemyQuantity; i++)
            {
                SpawnBoss(boss.useRandomPosition, boss.specificPositionSpawn, boss.enemyController);
                yield return new WaitForSeconds(boss.enemySpawnCadence);
            }

            yield return new WaitForSeconds(waveSpawnData.waveCandence);
        }

    }

    private void SpawnBoss(bool useRandomPosition, Vector3 specificPositionSpawn, EnemyConfigController enemyController)
    {
        GameObject obj = bossPoolList[0].getPooledObject();
        obj.transform.position = specificPositionSpawn;
        obj.GetComponent<EnemyConfigController>().SetConfig(enemyController.enemyConfig);
        obj.SetActive(true);
    }

    private void SpawnEnemy(bool randomPosition, Vector3 specificPosition, EnemyConfigController enemyController)
    {
        if (randomPosition)
        {
            SpawnPooledEnemyFromRandomArea(enemyController);
        } else
        {
            if (specificPosition != null)
            {
                SpawnPooledEnemyFromSpecificPosition(specificPosition, enemyController);
            }
        }
    }

    public void SpawnPooledEnemyFromRandomArea(EnemyConfigController enemyController)
    {
        Vector3 position = enemySpawnT.GetRandomPosition();
        SpawnPooledEnemyFromSpecificPosition(position, enemyController);

    }

    public void SpawnPooledEnemyFromSpecificPosition(Vector3 specificPosition, EnemyConfigController enemyController)
    {
        GameObject obj = enemyPoolList[0].getPooledObject();
        obj.transform.position = specificPosition;
        obj.GetComponent<EnemyConfigController>().SetConfig(enemyController.enemyConfig);
        obj.SetActive(true);

    }


}

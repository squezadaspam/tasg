﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIGameOverController : MonoBehaviour
{
    [SerializeField]
    private Button backButton;


    private MainGameControllerCore mainGameController;
    // Start is called before the first frame update
    void Start()
    {
        mainGameController = GameObject.Find("MainGameControllerCore").GetComponent<MainGameControllerCore>();

        backButton.onClick.AddListener(Back);
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Back()
    {
        mainGameController.GoToStartScreen();
    }
}

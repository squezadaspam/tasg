﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDisableThis : MonoBehaviour
{
    public void DisableThis()
    {
        gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttributes
{
    private float life = 100;
    private float lifeCount = 0;
    private float speed = 5.6f;
    private float speedCount =0;
    private float shield = 10;
    private float damage = 100;
    private float damageCount = 0;
    private float candence = 0.1f;
    private float candenceCount = 0;
    private bool isDead = false;

    public float Life { get => life; set => life = value; }
    public float Speed { get => speed; set => speed = value; }
    public float Damage { get => damage; set => damage = value; }
    public float Shield { get => shield; set => shield = value; }
    public float Candence { get => candence; set => candence = value; }
    public float LifeCount { get => lifeCount; set => lifeCount = value; }
    public float SpeedCount { get => speedCount; set => speedCount = value; }
    public float DamageCount { get => damageCount; set => damageCount = value; }
    public float CandenceCount { get => candenceCount; set => candenceCount = value; }

    public PlayerAttributes()
    {

    }

    public void TakeDamage(float hit)
    {
        //operations for take damage thinking in the shield points
        isDead = true;
    }

    public bool IsDead()
    {
        return this.isDead;
    }

    


}

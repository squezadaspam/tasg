﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyConfigController : MonoBehaviour
{
    public EnemyConfig enemyConfig;
    public SpawnZone topSpawnZone;
    public SpawnZone enemyAreaSpawnZone;

    private Vector3 selectedRandomPosition;
    private void Start()
    {
        topSpawnZone = GameObject.Find("T").GetComponent<SpawnZone>();
        enemyAreaSpawnZone = GameObject.FindGameObjectWithTag("EnemyArea").GetComponent<SpawnZone>();

        if (enemyConfig.randomSpecificPosition)
        {
            selectedRandomPosition = enemyAreaSpawnZone.GetRandomPosition();
        }
    }
    private void FixedUpdate()
    {
        PerformMovement();
    }

    private void PerformMovement()
    {
        if (enemyConfig.moveToSpecificPoint)
        {
           
            MoveToPoint(enemyConfig.specificPosition);
        }
        else
        {
            if (enemyConfig.randomSpecificPosition)
            {
                Debug.Log(selectedRandomPosition);
                MoveToPoint(selectedRandomPosition);
                return;
            }
            Move();
        }
    }

    private void Move()
    {
        transform.position += enemyConfig.enemyDirection * enemyConfig.enemySpeed * Time.deltaTime;
    }

    private void MoveToPoint(Vector3 targetPosition)
    {
        float step = enemyConfig.enemySpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
    }

    public void SetConfig(EnemyConfig enemyConfig)
    {
        this.enemyConfig = enemyConfig;
    }

    public void Shooting()
    {
        GameObject Player = GameObject.FindGameObjectWithTag("Player");

    }
}

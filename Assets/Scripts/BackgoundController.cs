﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgoundController : MonoBehaviour
{
    [SerializeField]
    private float scrollSpeed = 0.5f;
    [SerializeField]
    private MeshRenderer meshRender;
    // Start is called before the first frame update
    void Start()
    {
        meshRender = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        ScrollBackground();
    }

    void ScrollBackground()
    {
        Vector2 offset = new Vector2(0,Time.time * scrollSpeed);
        meshRender.material.mainTextureOffset = offset;
    }
}

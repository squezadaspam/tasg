﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{

    public static float GetAngleFromVectorFloat(Vector3 dir)
    {
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;

        return n;

    }

    /**
     * Obtiene la posición relativa referente al objeto "origin"
     * Fuente: https://answers.unity.com/questions/346671/relative-position-1.html
     */
    public static Vector3 getRelativePosition(Transform origin, Vector3 position)
    {
        Vector3 distance = position - origin.position;
        Vector3 relativePosition = Vector3.zero;
        relativePosition.x = Vector3.Dot(distance, origin.right.normalized);
        relativePosition.y = Vector3.Dot(distance, origin.up.normalized);

        return relativePosition;
    }

}

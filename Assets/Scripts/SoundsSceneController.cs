﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SoundsSceneController : MonoBehaviour
{
    public AudioClip startScreen;
    public AudioClip garageScreen;
    public AudioClip firstLevelScreen;
    public AudioClip bossScreen;

    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySceneSound(string clipId)
    {
        audioSource.Stop();
        try
        {
            
            switch (clipId)
            {
                case "Start":
                    audioSource.clip = startScreen;
                    audioSource.loop = true;
                    audioSource.Play();
                    break;
                case "Garage":
                    audioSource.clip = garageScreen;
                    audioSource.loop = true;
                    audioSource.Play();
                    break;
                case "FirstLevel":
                    audioSource.clip = firstLevelScreen;
                    audioSource.loop = true;
                    audioSource.Play();
                    break;
                case "Boss":
                    audioSource.clip = bossScreen;
                    audioSource.loop = true;
                    audioSource.Play();
                    break;
            }
            
        }catch (Exception e)
        {
            Debug.Log(e);
        }
        
    }
}

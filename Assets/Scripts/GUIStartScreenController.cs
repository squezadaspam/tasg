﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIStartScreenController : MonoBehaviour
{
    [SerializeField]
    private Button startButton;

    [SerializeField]
    private Button garageButton;

    [SerializeField]
    private Button optionsButton;

    [SerializeField]
    private Button exitButton;

    private MainGameControllerCore mainGameController;
    // Start is called before the first frame update
    void Start()
    {
        mainGameController = GameObject.Find("MainGameControllerCore").GetComponent<MainGameControllerCore>();

        startButton.onClick.AddListener(PlayGame);
        garageButton.onClick.AddListener(GoToGarage);
        optionsButton.onClick.AddListener(GoToOptions);
        exitButton.onClick.AddListener(ExitGame);
    }
    private void PlayGame()
    {
        mainGameController.StartNewGame();
        Debug.Log("PLAY");
    }

    private void GoToGarage()
    {
        mainGameController.GoToGarageScreen();
        Debug.Log("Garage");
    }

    private void GoToOptions()
    {
        mainGameController.GoToOptionScreen();
    }

    private void ExitGame()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }
}
